# First-Step

For Ubuntu 20.04 minimal install:
-----
* [x] remove Eye Of Gnome
* [x] install software from repository
* [x] remove Snap
* [x] install Gnome Software Center
* [x] install from ppa:
    - [x] Bashtop
    - [x] Celluloid
    - [x] Wine Staging
    - [x] Lutris
    - [ ] Nvidia *(optional)*
* [ ] install Vulkan *(optional)*
* [x] download LibreOffice as AppImage