#!/bin/bash

clear
echo -e "\x1B[01;31mSetup script for Ubuntu 20.04 minimal install \x1B[0m"

# remove Eye Of Gnome
echo -e "\x1B[01;32m Removing Eye Of Gnome\x1B[0m"
read -p "Press enter to continue"
sudo apt remove eog -y

# install a lot of software
echo -e "\x1B[01;32m Now, lets install some software from main repo: \x1B[0m"
read -p "Press enter to continue"
sudo apt install neofetch gthumb thunderbird firefox grsync htop git pdfshuffler pavucontrol \
gedit-plugin-text-size libreoffice synaptic brasero rhythmbox gnome-tweaks obs-studio ffmpeg \
kdenlive vlc filezilla steam ubuntu-restricted-extras -y

# install Bashtop
echo -e "\x1B[01;32m Add Bashtop ppa and install \x1B[0m"
read -p "Press enter to continue"
sudo add-apt-repository ppa:bashtop-monitor/bashtop -y
sudo apt install bashtop -y

# install Celluloid
echo -e "\x1B[01;32m Add Celluloid ppa and install \x1B[0m"
read -p "Press enter to continue"
sudo add-apt-repository ppa:xuzhen666/gnome-mpv -y
sudo apt install celluloid -y

# install Wine
echo -e "\x1B[01;32m Installing Wine: \x1B[0m"

echo -e "\x1B[01;34m   -add i386 architecture \x1B[0m"
read -p "Press enter to continue"
sudo dpkg --add-architecture i386

echo -e "\x1B[01;34m   -add Wine key \x1B[0m"
read -p "Press enter to continue"
wget -O - https://dl.winehq.org/wine-builds/winehq.key | sudo apt-key add -

echo -e "\x1B[01;34m   -install Wine depndencies \x1B[0m"
read -p "Press enter to continue"
sudo apt install libgnutls30:i386 libldap-2.4-2:i386 libgpg-error0:i386 libxml2:i386 \
libasound2-plugins:i386 libsdl2-2.0-0:i386 libfreetype6:i386 libdbus-1-3:i386 libsqlite3-0:i386 -y

echo -e "\x1B[01;34m   -add Wine ppa and install \x1B[0m"
read -p "Press enter to continue"
sudo add-apt-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ focal main' -y
sudo apt install --install-recommends winehq-staging -y

# install Lutris
echo -e "\x1B[01;32m Install Lutris \x1B[0m"

read -p "Press enter to continue"
sudo add-apt-repository ppa:lutris-team/lutris -y
sudo apt install lutris -y

# install nVidia 450 and Vulkan
echo -e "\x1B[01;32m Add nVidia new drivers and install \x1B[0m"

echo -e "\x1B[01;34m   -add nVidia drivers ppa \x1B[0m"
read -p "If you dont have nvidia card press N"
sudo add-apt-repository ppa:graphics-drivers/ppa -y
sudo apt install nvidia-driver-450 libnvidia-gl-450 libnvidia-gl-450:i386

echo -e "\x1B[01;34m   -install Vulkan libaries \x1B[0m"
read -p "If your card is not Vulkan comapatible press N"
sudo apt install libvulkan1 libvulkan1:i386

# install gnome software center and remove snap
echo -e "\x1B[01;32m Install gnome software center and remove snap\x1B[0m"
read -p "Press enter to continue"

echo -e "\x1B[01;32m Installing gnome-software\x1B[0m"
sudo apt install gnome-software -y
echo -e "\x1B[01;32m Delete snapd from /var/cache/\x1B[0m"
sudo rm -rf /var/cache/snapd/
echo -e "\x1B[01;32m Remove snapd\x1B[0m"
sudo apt autoremove --purge snapd gnome-software-plugin-snap -y
echo -e "\x1B[01;32m Delete snap from users home directory\x1B[0m"
sudo rm -rf ~/snap

# download LibreOffice as AppImage in ~/AppImage/
echo -e "\x1B[01;32m Download LibreOffice as AppImage\x1B[0m"
read -p "Press enter to continue"
mkdir ~/AppImage
pushd ~/AppImage/
wget https://libreoffice.soluzioniopen.com/stable/full/LibreOffice-fresh.full-x86_64.AppImage
chmod +x LibreOffice-fresh.full-x86_64.AppImage
popd

echo -e "\x1B[01;32m*** All done! Please\x1B[0m""\x1B[01;31m update\x1B[0m""\x1B[01;32m and\x1B[0m" "\x1B[01;31mreboot\x1B[0m""\x1B[01;32m! ***\x1B[0m"
exit
